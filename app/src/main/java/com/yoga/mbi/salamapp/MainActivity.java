package com.yoga.mbi.salamapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.yoga.mbi.salamapp.LoginActivity.token;

public class MainActivity extends AppCompatActivity {

    private Uri fileUri;
    private int REQUEST_FOTO = 100;
    private String picturePath;
    private Bitmap bitmap;
    private static final String API_LAPORAN = "http://salam.mbi.biz.id/api/report" ;
    private EditText edtTextketerangan;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtTextketerangan = (EditText) findViewById(R.id.editTextKeterangan);
        Button uploadButton = (Button) findViewById(R.id.buttonUpload);
        Button kirimLaporan = (Button) findViewById(R.id.btnKeterangan);

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getApplicationContext().getPackageManager().hasSystemFeature(
                        PackageManager.FEATURE_CAMERA)) {

                    picturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/picture.jpg";
                    File imageFile = new File(picturePath);
                    Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri

                    Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    it.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
                    startActivityForResult(it, REQUEST_FOTO);
                } else {
                    Toast.makeText(getApplication(), "Camera not supported", Toast.LENGTH_LONG).show();
                }
            }
        });

        kirimLaporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SendLaporanData().execute(edtTextketerangan.getText().toString());
                Toast.makeText(getApplication(),"Laporan Berhasil Terkirim", Toast.LENGTH_LONG).show();

            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_FOTO && resultCode == RESULT_OK) {
            ImageView mImagePreview = (ImageView) findViewById(R.id.imagePrev);

            // Decode it for real
            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = false;

            //imageFilePath image path which you pass with intent
            bitmap = BitmapFactory.decodeFile(picturePath, bmpFactoryOptions);

            // Display it
            mImagePreview.setImageBitmap(bitmap);
        }
    }


    private class SendLaporanData extends AsyncTask<String, Void, String> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @SuppressWarnings("deprecation")
        @Override
        protected String doInBackground(String... arg) {
            //String token = arg[0];
            String keterangan = arg[0];


            // Image

            String ba1 = null;
            Log.d("gambar","bitmap = " + bitmap);

            if ( bitmap != null ) {
                Bitmap bm = BitmapFactory.decodeFile(picturePath);
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                byte[] ba = bao.toByteArray();
                ba1 = Base64.encodeToString(ba, Base64.NO_WRAP);
            }
            Log.d("gambar","ba1 = " + ba1);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("token", token));
            params.add(new BasicNameValuePair("keterangan", keterangan));
            params.add(new BasicNameValuePair("foto", "data:image/png;base64," + ba1));
            //params.add(new BasicNameValuePair("ImageName", System.currentTimeMillis() + ".jpg"));
            Log.d("asli","params: " + params);
            ServiceHandler serviceClient = new ServiceHandler();
            String json = serviceClient.makeServiceCall(API_LAPORAN, ServiceHandler.POST, params);

            String result = null;
            //String token = null;
            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);
                    result = jsonObj.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("JSON Data", "Didn't receive any data from server!");
            }

            Log.d("Cek-json", "result:"+result);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing())
                pDialog.dismiss();

            if (result.equals("success")){
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(getApplication(),"Laporan Belum Terkirim", Toast.LENGTH_LONG).show();
            }
        }
}
}
