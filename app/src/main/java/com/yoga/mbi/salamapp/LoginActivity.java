package com.yoga.mbi.salamapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;




public class LoginActivity extends AppCompatActivity {

    private static final String API_LOGIN = "http://salam.mbi.biz.id/api/login";
    public static final String mypreference = "mypref";
    public static String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText userLogin = (EditText) findViewById(R.id.editTextUsername);
        final EditText userPassword = (EditText) findViewById(R.id.editTextPassword);

        Button buttonlogin = (Button) findViewById(R.id.btnLogin);
        Button loginToRegister = (Button) findViewById(R.id.btnToRegister);

        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SendLoginData().execute(userLogin.getText().toString(),
                        userPassword.getText().toString());

            }
        }
        );

        loginToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(intent);
            }
        });
}

    private void successLogin() {
        MainActivity.start(this);
        finish();
    }

    private class SendLoginData extends AsyncTask<String, Void, String> {

        private ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @SuppressWarnings("deprecation")
        @Override
        protected String doInBackground(String... arg) {
            String username = arg[0];
            String password = arg[1];

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));

            ServiceHandler serviceClient = new ServiceHandler();
            String json = serviceClient.makeServiceCall(API_LOGIN, ServiceHandler.POST, params);

            //Log.d("Cek-json", "result:"+result);
            return json;


         //   SharedPreferences prefs = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
          //  SharedPreferences.Editor editor = prefs.edit();
          //  editor.putString("token", token);
          //  editor.apply();


        }



        @Override
        protected void onPostExecute(String json) {
            if (pDialog.isShowing())
                pDialog.dismiss();

            String status = null;
            token = null;
            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);
                    status =jsonObj.getString("status");
                    token = jsonObj.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("JSON Data", "Didn't receive any data from server!");
            }
            Log.d("asli","status: " + status);
            Log.d("asli","token: " + token);


            if (status.equals("success")){
                successLogin();
            }else {
                Toast.makeText(getApplication(),"Username salah", Toast.LENGTH_LONG).show();
            }
        }




    }}
