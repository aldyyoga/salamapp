package com.yoga.mbi.salamapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    private static final String API_REGISTER = "http://salam.mbi.biz.id/api/register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText userUsername = (EditText) findViewById(R.id.editTextUsernameRegister);
        final EditText userPassword = (EditText) findViewById(R.id.editTextPasswordRegister);
        final EditText userPhone = (EditText) findViewById(R.id.ediTextPhoneRegister);
        final EditText userAddress = (EditText) findViewById(R.id.editTextAddressRegister);

        Button registerToLogin = (Button) findViewById(R.id.btnToLogin);
        Button btnRegister = (Button) findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SendRegisterData().execute(userUsername.getText().toString(),
                        userPassword.getText().toString(),userPhone.getText().toString(),userAddress.getText().toString());
                Toast.makeText(getApplication(),"Register Berhasil", Toast.LENGTH_LONG).show();
            }
        });

        registerToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
            }
        });
    }

    private class SendRegisterData extends AsyncTask<String, Void, String> {

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @SuppressWarnings("deprecation")
        @Override
        protected String doInBackground(String... arg) {
            String username = arg[0];
            String password = arg[1];
            String phone = arg[2];
            String address = arg[3];

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));
            params.add(new BasicNameValuePair("phone", phone));
            params.add(new BasicNameValuePair("address", address));

            ServiceHandler serviceClient = new ServiceHandler();
            String json = serviceClient.makeServiceCall(API_REGISTER, ServiceHandler.POST, params);

            String result = null;
            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);
                    result =jsonObj.getString("status");
                    result =jsonObj.getString("error");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("JSON Data", "Didn't receive any data from server!");
            }

            Log.d("Cek-json", "result:"+result);
            Log.d("cek-param", "result:"+params);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing())
                pDialog.dismiss();

            if (result.equals("success")){
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(getApplication(),"Register berhasil", Toast.LENGTH_LONG).show();
            }
        }
    }}

